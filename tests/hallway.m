
south_wall = lineObject;
south_wall.lines = [...
	-17 200;
	-17 122.25;
	0   122.25;
	0   0;
	160 0;
	160 -24].*.0254;


north_wall = lineObject;
north_wall.lines = [...
	81.75   200;
	81.75   113.5;
	209.75  113.5;
	209.75  99.5;
	234.75  99.5;
	234.75 -24].*.0254;

map_objects = [south_wall north_wall];
%%
%Initialize the robot
rob = neato('sim', [1 1 0]');
pause(.3);

%Generate the map with the objects in it.  The map to raycast againist in
%an attribute of the robot class
rob.genMap( map_objects );

%Start up the laser
rob.startLaser();
pause(.3);
