% must have PsychToolbox (specifically KbCheck) for this to work

cl = 0;
cr = 0;
keys =[];

while(all(keys~=22))
    [keyIsDown, secs, keyCode, deltaSecs] = KbCheck(); 
    keys = find(keyCode);
    
    vl = 0;
    vr = 0;
    
    if(any(keys == 112) || any(keys == 82))
        vl = vl + .2;
        vr = vr + .2;
    end
    if(any(keys == 114) || any(keys == 80))
        vl = vl - .1;
        vr = vr + .1;
    end
    if(any(keys == 115) || any(keys == 79))
        vl = vl + .1;
        vr = vr - .1;
    end
    if(any(keys == 117) || any(keys == 81))
        vl = vl - .2;
        vr = vr - .2;
    end
    
    cl = (cl*.8) + (vl*.2);
    cr = (cr*.8) + (vr*.2);
    
    rob.sendVelocity(cl,cr);

    ranges = rob.laser.data.ranges;
    px = [ranges .* cos(deg2rad(0:359)); ranges.*0];
    py = [ranges .* sin(deg2rad(0:359)); ranges.*0];
    
    set(ph,'XData',px(:), 'YData', py(:));
    
    pause(.1);
end
rob.sendVelocity(0,0);
rob.sendVelocity(0,0);
