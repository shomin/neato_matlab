if(exist('rob'))
    try
        rob.shutdown();
    catch
        clear rob
    end
end

close all
delete(timerfindall);
clear classes
pause(.1);



rob = neato('sim'); 



ob1 = lineObject;
ob1.lines = [...
	-2  1.5;
	4   1.5;
	4  -1.5;
    -2 -1.5;
	-2  1.5];

ob2 = lineObject;
ob2.lines = .2.*[-1 1;-1 -1;1 -1;1 1;-1 1];
ob2.color = [.4 .7 .2];
t = (0:.01:4*pi)';
ob2.path = [2+1.2*cos(.5*t) .8*sin(.5*t) t t];
ob2.cyclic = true;

ob3 = lineObject;
ob3.lines = .1.*[-1 1;-1 -1;1 -1;1 1;-1 1];
ob3.color = [.8 .4 .8];
t = (0:.01:2*pi)';
ob3.path = [2+.6*cos(t) -.4*sin(t) t t];
ob3.cyclic = true;

map_objects = [ob1 ob2 ob3];

pause(.3);
rob.genMap( map_objects );

rob.startLaser();
pause(.3);
%%

figure(2);clf;hold off
ph = plot(0,0,'r-');
grid on 
axis equal
axis([-5 5 -5 5]);
%%
while 1
    ranges = rob.laser.data.ranges;
    px = [ranges .* cos(deg2rad(0:359)); ranges.*0];
    py = [ranges .* sin(deg2rad(0:359)); ranges.*0];
    
    set(ph,'XData',px(:), 'YData', py(:));
    pause(.1);
end