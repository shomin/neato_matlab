
%Make walls (2 6ft walls)
walls = lineObject;
walls.lines = [...
	0  6;
	0  0;
	6  0].*(12*.0254);

%Make a box (centered at origin), then set it's pose
box = lineObject;
box.lines = [...
	-.2  .2;
	-.2 -.2;
	 .2 -.2;
	 .2  .2;
	-.2  .2];
box.color = [1 0 1];
box.pose = [4 1 .3];

%Put all the map objects into an array
map_objects = [walls box];

map = lineMap(map_objects);

hold on

h = plot(0,0,'g-', 'LineWidth', 3);

%% Click the map to "highlight" the closes object-line
% and show the distance to it.  Right click to exit the loop
[cx, cy, button] = ginput(1);
while button == 1
	[dist,ob_num,line_num] = map.closestObject([cx;cy]);
	dist
	line_coords = map.objects(ob_num).line_coords(line_num:line_num+1,:);
	set(h, 'XData', line_coords(:,1), 'YData', line_coords(:,2));
	[cx, cy, button] = ginput(1);
end