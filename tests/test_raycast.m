
% All of these lines are to deal with leftover classes, etc.
if(exist('rob'))
    try
        rob.shutdown();
    catch
        clear rob
    end
end
close all
delete(timerfindall);
clear classes
pause(.1);

%Initialize the robot
rob = neato('sim', [1 0 .8]');
pause(.3);

%Make a wall
ob1 = lineObject;
ob1.lines = [...
	0  1;
	4  1;
	4 -3];

%Make another wall
ob2 = lineObject;
ob2.lines = [...
	0  -1;
	2  -1;
	2  -3];

%Make a box (centered at origin), then set it's pose
ob3 = lineObject;
ob3.lines = [...
	-.2  .2;
	-.2 -.2;
	 .2 -.2;
	 .2  .2;
	-.2  .2];
ob3.color = [1 0 0];
ob3.pose = [3 .5 0];

%Put all the map objects into an array
map_objects = [ob1 ob2 ob3];

%Generate the map with the objects in it.  The map to raycast againist in
%an attribute of the robot class
rob.genMap( map_objects );

%Start up the laser
rob.startLaser();
pause(.3);

%% Use another figure to look at the data

figure(2);clf;hold on

%Plot the robot
step = pi/20;q1 = 0:step:pi/2;q2 = pi/2:step:pi;cir = 0:step:2*pi;
lx = (.04*-cos(cir)) -.1;
ly = .04*sin(cir);
bx = [-sin(q1)*.165 lx [-sin(q2) 1  1  0]*.165];
by = [-cos(q1)*.165 ly [-cos(q2) 1 -1 -1]*.165];
plot(bx,by,'-','LineWidth',2,'Color',[.02 .3 .05]);

%Make a plot handle for the laser data
ph = plot(0,0,'r-');
hold off
grid on 
axis equal
axis([-5 5 -5 5]);

%% Random Walk and Update Laser Plot (no callback)

filt_vel = [0 0];

while 1
	
	%grab the ranges
    ranges = rob.laser.data.ranges;
    px = [ranges .* cos(deg2rad(0:359)); ranges.*0];
    py = [ranges .* sin(deg2rad(0:359)); ranges.*0];

	%plot them
    set(ph,'XData',px(:), 'YData', py(:));

	%get a (semi)random velocity, filter it and send it to the robot
    vel = .05+rand(2,1)'*.1;
    filt_vel = .6.*filt_vel + .4.*vel;
    rob.sendVelocity(filt_vel(1), filt_vel(2));
    pause(.3);
end
