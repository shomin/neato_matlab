clear all; close all;

ob1 = lineObject;
ob1.lines = [...
	.5  .7;
	-.5 .7;
	-.5 -.7;
	.5 -.7];
% 	.5 .7];

ob1.pose = [1 1 .9];

map = lineMap(ob1);

ob2 = lineObject;
ob2.lines = [-.3 0;	0  0];
ob2.color = [.8 .2 .2];

ob2.path = [...
	1    -1   0   0;
	1.2 -1.2  .7  1;
	1.8 -1.4  1.5 2;
	2.8 -1.6  2.2 3];
oid = map.addObject(ob2);


ob3 = lineObject;
ob3.lines = [...
	-.3 0;
	0  0;
	0  .3;
	-.3 0];
ob3.color = [.4 .9 .2];
ang = 0:.01:2*pi;
ob3.path = [ang'.*0 ang'.*0 ang' ang'];
ob3.cyclic = true;

map.addObject(ob3);


while true
	map.update();
	pause(.05);
end
