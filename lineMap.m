classdef lineMap < handle
    %robotPose A layer over a column vector that provides access methods

    properties(GetAccess = public, SetAccess = private)
		num_objects = 0;
		objects;
		fig_num;
	end
            
    methods(Access = public)
        
        function lm = lineMap(objects, fig_num)
			if( nargin < 1)
				error('You need to intialize lineMap with at least one object');
			end
			if(nargin > 1 )
				lm.fig_num = figure(fig_num);
			else
				lm.fig_num = figure();
			end
			for obj = objects
				lm.addObject(obj);
			end
		end
        
		function n = addObject(lm, object)
			if( isempty(object.lines))
				error('Object must have line of size n x 2 (n>0)');
			else				
				if ~isempty(object.path)
					object.has_path = true;
					object.pose = object.path(1,1:3)';
				else
					object.has_path = false;
				end
				object.startTimer();
				
				figure(lm.fig_num); hold on;
				object.update();
				object.plot();
				hold off;
				axis equal

				if isempty(lm.objects)
					lm.objects = object;
				else
					lm.objects(end + 1) = object;
				end
				lm.num_objects = lm.num_objects + 1;
				lm.objects(end).id = lm.num_objects;
				n = lm.num_objects;
			end
		end
		
		function ranges = raycast(lm, pose, max_range, ang_range)
            sweep = pose(3) + deg2rad(ang_range);
            
            p0x = pose(1);
            p0y = pose(2);
            
            p1x = p0x + max_range.*cos(sweep);
            p1y = p0y + max_range.*sin(sweep);
			
            s1x = p1x - p0x;
            s1y = p1y - p0y;
            
            lc = cell2mat({lm.objects(:).line_coords}');
            
            p2x = lc(1:end-1,1);
            p2y = lc(1:end-1,2);
            
            p3x = lc(2:end,1);
            p3y = lc(2:end,2);
            
            s2x = p3x - p2x; 
            s2y = p3y - p2y;

            s = ((p2x-p0x)*s1y + (p0y-p2y)*s1x)./...
                (-s2x*s1y + s2y*s1x);
            
			t = repmat(( s2x.*(p0y-p2y) - s2y.*(p0x-p2x)),1,length(s1x))...
                ./ (-s2x*s1y + s2y*s1x);

            col = s >= 0 & s <= 1 & t >= 0 & t <= 1;
            t(~isfinite(t)) = 0;
            cpx = (t .* repmat(s1x,length(s2x),1)) .* col;
            cpy = (t .* repmat(s1y,length(s2x),1)) .* col;
            r = (cpx.^2 + cpy.^2).^.5;
            
            r(~col) = nan;
            
            ng = cumsum(cellfun(@(x) size(x,1), {lm.objects(:).line_coords}'));
            
            r(ng(1:end-1),:) = [];
            
            ranges = min(r);
            ranges( isnan(ranges) ) = 0;

		end
		
		function [dist, ob_num, line_num] = closestObject(lm, pos)

            lc = cell2mat({lm.objects(:).line_coords}');
			
			if( all(size(pos) == [1 2]) )
				P = pos;
			elseif( all(size(pos) == [2 1]) )
				P = pos';
			else
				error('pos must be 2x1');
			end
			
			%line endpoints
			P0 = lc(1:end-1,:);
			P1 = lc(2:end,:);
			
			%Query Point
			P = repmat(P, size(P0,1),1);
			
			D = zeros(size(P0,1),1);
			
			%useful vectors
			v = P1 - P0;
			w = P - P0;

			%dot products to check case
			c1 = sum(v.*w,2);
			c2 = sum(v.*v,2);
			
			%before P0
			bp0 = c1<=0;
			D(bp0) = sqrt(sum((P(bp0,:) - P0(bp0,:)).^2,2));
			
			%after P1
			ap1 = c2<=c1;
			D(ap1) = sqrt(sum((P(ap1,:) - P1(ap1,:)).^2,2));
			
			%perpedicular to line
			per = ~(bp0 | ap1);
			
			b = c1 ./ c2;
			Pb = P0 + ([b b].*v);
			D(per) = sqrt(sum((P(per,:) - Pb(per,:)).^2,2));
					
			%superfluous (nonexistent lines)
			ng = cumsum(cellfun(@(x) size(x,1), {lm.objects(:).line_coords}'));
			D(ng(1:end-1)) = inf;

			%get minimum distance
			[dist, ind] = min(D);
			
			%find the object and line number
			zng = [0;ng];
			ob_num = find(ind > zng, 1, 'last');
			line_num = ind - zng(ob_num);
		end
		
		function lm = removeObject(lm, i)
			lm.objects(i).lines = [];
			delete(lm.objects(i).h)
			lm.num_objects = lm.num_objects - 1;
		end
				
		function lm = update(lm)
			for i = 1:length(lm.objects)
				lm.objects(i).update();
				lm.objects(i).plot();
			end
		end
    end
end