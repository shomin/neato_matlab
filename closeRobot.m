function closeRobot(robot)
%function closeRobot(robot)
% Shut down robot and kill the websocket connection.  
robot.close();
end

