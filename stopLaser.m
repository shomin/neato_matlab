function stopLaser(robot)
%function stopLaser(robot)
% Stop the Laser Rotation, other publishers will be able to publish faster
% with the laser turned off

robot.stopLaser();

end
