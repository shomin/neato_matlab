classdef robotPose < handle
    %robotPose A layer over a column vector that provides access methods
    
    properties(Constant)

    end
    
    properties(Access = private)
        pose = [0 ; 0 ; 0];
    end
    
    properties(Access = public)

    end
    
    methods(Static = true)
        
    end
    
    methods(Access = private)
         
    end
            
    methods(Access = public)
        
        function obj = robotPose(x,y,th)
            obj.pose = [x ; y ; th];
        end
        
        function x = x(obj);   x   = obj.pose(1);  end        
        function y = y(obj);   y   = obj.pose(2);  end      
        function th = th(obj); th  = obj.pose(3);  end         
    end
end