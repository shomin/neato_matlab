function startLaser(robot)
%function startLaser(robot)
% Start the Laser Rotation, will halt other packets for a few seconds while
% spinning up.  Note that running the laser will slow the rate of the other
% publishers.

robot.startLaser();
end
