function sendVelocity(robot, vel_left, vel_right)
%function sendVelocity(robot, vel_left, vel_right)
% send a velocity command to the robot.  Send no faster than 20 Hz, or you
% will slow down the whole control loop
% vel_left and vel_right are in m/s
robot.sendVelocity(vel_left, vel_right);
end
